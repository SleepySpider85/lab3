
import org.joda.time.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet(
		description = "Just sayinghello and some other bits and bobs", 
		urlPatterns = { 
				"/HelloServlet", 
				"/hello"
		})
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DateTime dateTime = new DateTime();
		int daysLeftInMonth= dateTime.getDayOfMonth();
		int daysLeftInYear =  dateTime.dayOfMonth().getMaximumValue() - dateTime.getDayOfMonth();
		
		StringBuffer buf = new StringBuffer();
		buf.append("<HTML><HEAD><TITLE>\n");
		buf.append("Hello Servlet\n");
		buf.append("</TITLE></HEAD><BODY>\n");
		buf.append("<H1>Welcome to OOSSP!</H1>\n");
		buf.append("<p>Number Of Days Left In Month : " + daysLeftInMonth + " </p>");
		buf.append("<p>Number Of Days Left In Year: " + daysLeftInYear + " </p>");
		buf.append("<p>Name : David Coughlan </p>");
		buf.append("<p>Student Number : R00009964</p>");
		buf.append("</BODY></HTML>");
				
		response.getWriter().println(buf.toString());
		response.getWriter().close();

	}
	
	public static int daysOfMonth(int year, int month)
	{
		
		
		return 0;//dateTime.getDayOfMonth().getMaximumValue();
	}
}
